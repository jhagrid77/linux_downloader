#!/usr/bin/env bash

# Source: http://mywiki.wooledge.org/BashFAQ/035
die() {
    printf '%s\n' "$1" >&2
    exit 1
}

extension=".torrent"
downloader="wget -r -np -nd -c -q --show-progress -e robots=off"
output="."

# Download list
all=1
archlinux=0
centos=0
debian=0
fedora=0
kali=0
manjaro=0
mint=0
tails=0
ubuntu=0

# Links to download from
# Arch has iso and torrents together, this works with ftp only
arch_ftp=ftp://mirror.rackspace.com/archlinux/iso

# CentOS has iso and torrents together,  this works with ftp only
centos_ftp=ftp://mirror.rackspace.com/CentOS

# Debian has iso and torrents together, this works with ftp only
debian_ftp=ftp://cdimage.debian.org/debian-cd/current/amd64

# Fedora has iso and torrents seperate, fedora has a redirect to a mirror so curl is used
fedora_torrent=https://torrent.fedoraproject.org/torrents
fedora_iso=$(curl -Ls -o /dev/null -w %{url_effective} https://download.fedoraproject.org)

# Kali has iso and torrents seperate 
kali_iso=https://cdimage.kali.org/kali-images/current
kali_torrent=https://images.offensive-security.com

# Manjaro has iso and torrents together
manjaro_download=https://osdn.net/projects/manjaro/storage/xfce/

# Ubuntu has iso and torrents together, this works with ftp only
ubuntu_ftp=ftp://releases.ubuntu.com/releases

# Source: http://mywiki.wooledge.org/BashFAQ/035
while :; do
    case "$1" in
        -h | -\? | --help)
            echo "Automatically download latest linux distributions"
            echo "Usage:"
            echo "    ./linux_downloader.sh [options]"
            echo ""
            echo "General Options:"
            echo "    -h, --help               Display this help message"
            echo "    -e, --extension          Change the download type to either [iso,torrent], Default is torrent"
            echo "    -o, --output             Change the download directory, Default is current directory"
            echo "    --archlinux              Download Archlinux, will not download others unless selected"
            echo "    --centos                 Download CentOS,    will not download others unless selected"
            echo "    --debian                 Download Debian,    will not download others unless selected"
            echo "    --fedora                 Download Fedora,    will not download others unless selected"
            echo "    --kali                   Download Kali,      will not download others unless selected"
            echo "    --manjaro                Download Manjaro,   will not download others unless selected"
            echo "    --ubuntu                 Download Ubuntu,    will not download others unless selected"
            echo ""
            echo "Example:"
            echo "    ./linux_downloader.sh -e iso -o ~/Downloads/ --archlinux --ubuntu --fedora"
            echo ""
            exit 0
            ;;
        -e | --extension) # Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                extension=."$2"
                shift
            else
                die 'ERROR: "--extension" requires a non-empty option argument.'
            fi
            ;;
        -o | --output) # Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                output="$2"
                mkdir -p "$output"
                shift
            else
                die 'ERROR: "--extension" requires a non-empty option argument.'
            fi
            ;;
        --archlinux)
            all=0
            archlinux=1
            ;;
        --centos)
            all=0
            centos=1
            ;;
        --debian)
            all=0
            debian=1
            ;;
        --fedora)
            all=0
            fedora=1
            ;;
        --kali)
            all=0
            kali=1
            ;;
        --manjaro)
            all=0
            manjaro=1
            ;;
        --ubuntu)
            all=0
            ubuntu=1
            ;;
        --) # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *) # Default case: No more options, so break out of the loop.
            break ;;
    esac
    shift
done

# Archlinux
if [[ $all -eq 1 || $archlinux -eq 1 ]]; then
    echo "Downloading Archlinux"
    release=$(curl -l --silent "$arch_ftp"/ | sort -n -r | awk NR==1)

    if [ "$extension" == ".torrent" ]; then
        eval "$downloader" "$arch_ftp"/"$release"/*.torrent -P "$output"
    else
        while true; do
            eval "$downloader" "$arch_ftp"/"$release"/sha1sums.txt -P "$output"
            filename=$(grep .iso "$output"/sha1sums.txt | cut -d' ' -f3)
            checksum=$(grep .iso "$output"/sha1sums.txt | cut -d' ' -f1)

            eval "$downloader" "$arch_ftp"/"$release"/"$filename" -P "$output"
            filechecksum=$(sha1sum "$output"/"$filename" | cut -d' ' -f1)

            if [ "$checksum" = "$filechecksum" ]; then
                break
            fi

            echo "$filename failed, Redownloading"
            rm -f "$output"/"$filename"
        done

        rm -f "$output"/sha1sums.txt
    fi

    echo "Archlinux Done"
fi

# CentOS
if [[ $all -eq 1 || $centos -eq 1 ]]; then
    echo "Downloading Centos"
    release=$(curl -l --silent "$centos_ftp"/ | sort -n -r | awk NR==1)

    if [ "$extension" == ".torrent" ]; then
        eval "$downloader" "$centos_ftp"/"$release"/isos/x86_64/*.torrent -P "$output"
    else        
        eval "$downloader" "$centos_ftp"/"$release"/isos/x86_64/sha256sum.txt -P "$output"
        number=$(cat "$output"/sha256sum.txt | wc -l)

        for i in `seq 1 "$number"`; do
            while true; do
                filename=$(awk NR=="$i" "$output"/sha256sum.txt | cut -d' ' -f3)
                checksum=$(awk NR=="$i" "$output"/sha256sum.txt | cut -d' ' -f1)

                eval "$downloader" "$centos_ftp"/"$release"/isos/x86_64/"$filename" -P "$output"
                filechecksum=$(sha256sum "$output"/"$filename" | cut -d' ' -f1)

                if [ "$checksum" = "$filechecksum" ]; then
                    break
                fi

                echo "$filename failed, Redownloading"
                rm -f "$output"/"$filename"
            done
        done

        rm -f "$output"/sha256sum.txt
    fi

    echo "Centos Done"
fi

# Debian
if [[ $all -eq 1 || $debian -eq 1 ]]; then
    echo "Downloading Debian"

    if [ "$extension" == ".torrent" ]; then
        eval "$downloader" "$debian_ftp"/bt-cd/*.torrent -P "$output"
    else
        eval "$downloader" "$debian_ftp"/iso-cd/SHA512SUMS -P "$output"
        number=$(cat "$output"/SHA512SUMS | wc -l)

        for i in `seq 1 "$number"`; do
            while true; do
                filename=$(awk NR=="$i" "$output"/SHA512SUMS | cut -d' ' -f3)
                checksum=$(awk NR=="$i" "$output"/SHA512SUMS | cut -d' ' -f1)

                eval "$downloader" "$debian_ftp"/iso-cd/"$filename" -P "$output"
                filechecksum=$(sha512sum "$output"/"$filename" | cut -d' ' -f1)

                if [ "$checksum" = "$filechecksum" ]; then
                    break
                fi

                echo "$filename failed, Redownloading"
                rm -f "$output"/"$filename"
            done
        done

        rm -f "$output"/SHA512SUMS
    fi

    echo "Debian Done"
fi

# Fedora
if [[ $all -eq 1 || $fedora -eq 1 ]]; then
    echo "Downloading Fedora"
    if [ "$extension" == ".torrent" ]; then
        total=$(curl -l --silent "$fedora_torrent"/)
        workstation64=$(echo "$total" | grep 'Fedora-Workstation-'  | grep 'x86_64' | awk -F 'href="' '{print $2}' | cut -d'"' -f1 | sort -n -r | awk NR==1)
        workstation32=$(echo "$total" | grep 'Fedora-Workstation-' | grep 'i386' | awk -F 'href="' '{print $2}' | cut -d'"' -f1 | sort -n -r | awk NR==1)
        server64=$(echo "$total" | grep 'Fedora-Server-' | grep 'x86_64' | awk -F 'href="' '{print $2}' | cut -d'"' -f1 | sort -n -r | awk NR==1)
        server32=$(echo "$total" | grep 'Fedora-Server-' | grep 'i386' | awk -F 'href="' '{print $2}' | cut -d'"' -f1 | sort -n -r | awk NR==1)

        eval "$downloader" -A "*$extension" "$fedora_torrent"/{"$workstation32","$workstation64","$server64","$server32"} -P "$output"
    else
        fedora="${fedora_iso}"releases
        release=$(curl -l --silent "$fedora"/ | awk -F 'href="' '{print $2}' | cut -d'/' -f1 | sort -n -r | awk NR==1)
        fedora="$fedora"/"$release"

        distributions=("Everything" "Server" "Workstation")
        for type in ${distributions[@]}; do
            checkfile=$(curl --silent "$fedora"/"$type"/x86_64/iso/ | grep CHECKSUM | awk -F 'href="' '{print $2}' | cut -d'"' -f1)
            eval "$downloader" "$fedora"/"$type"/x86_64/iso/"$checkfile" -P "$output"
            number=$(awk '/# Fedora/{print $2}' "$output"/"$checkfile" | wc -l)

            for i in `seq 1 "$number"`; do
                while true; do
                    filename=$(awk '/# Fedora/{print $2}' "$output"/"$checkfile" | awk NR=="$i" | cut -d':' -f1)
                    checksum=$(awk '/ = /{print $(NF)}' "$output"/"$checkfile" | awk NR=="$i")
                    
                    eval "$downloader" "$fedora"/"$type"/x86_64/iso/"$filename" -P "$output"               
                    filechecksum=$(sha256sum "$output"/"$filename" | cut -d' ' -f1)

                    if [ "$checksum" = "$filechecksum" ]; then
                        break
                    fi

                    echo "$filename failed, Redownloading"
                    rm -f "$output"/"$filename"
                done
            done

            rm -f "$output"/"$checkfile"
        done
    fi

    echo "Fedora Done"
fi

# Kali
if [[ $all -eq 1 || $kali -eq 1 ]]; then
    echo "Downloading Kali"
    filename=$(curl -l --silent "$kali_iso"/ | awk -F 'href="' '{print $2}' | cut -d'"' -f1 | grep amd64 | sort -n | awk NR==1)

    if [ "$extension" == ".torrent" ]; then
        eval "$downloader" "$kali_torrent"/"$filename".torrent -P "$output"
    else
        while true; do
            eval "$downloader" "$kali_iso"/SHA256SUMS -P "$output"
            checksum=$(grep "$filename" "$output"/SHA256SUMS | cut -d' ' -f1)
            
            eval "$downloader" "$kali_iso"/"$filename" -P "$output"
            filechecksum=$(sha256sum "$output"/"$filename" | cut -d' ' -f1)

            if [ "$checksum" = "$filechecksum" ]; then
                break
            fi

            echo "$filename failed, Redownloading"
            rm -f "$output"/"$filename"
        done

        rm -f "$output"/SHA256SUMS
    fi

    echo "Kali Done"
fi

# Manjaro
if [[ $all -eq 1 || $manjaro -eq 1 ]]; then
    echo "Downloading Manjaro"
    filepath=$(curl -l --silent https://osdn.net/projects/manjaro/storage/xfce/ | awk -F 'href="' '{print $2}' | cut -d'"' -f1 | grep -e ^/projects/manjaro/storage/xfce | sort -n | grep -v "rc\|pre" | tail -n 1)
    filename=$(curl -l --silent https://osdn.net${filepath} | awk -F 'href="' '{print $2}'  | cut -d'"' -f1 | grep x86_64 | grep iso | cut -d'/' -f7 | awk -F '.iso' '{print $1}' | awk NR==1)

    if [ "$extension" == ".torrent" ]; then
        eval "$downloader" https://osdn.net${filepath}/"$filename".torrent -P "$output"
    else
        while true; do
            eval "$downloader" https://osdn.net${filepath}.sha256 -P "$output"
            checksum=$(grep "$filename" "$output"/SHA256SUMS | cut -d' ' -f1)
            
            eval "$downloader" https://osdn.net${filepath}/"$filename" -P "$output"
            filechecksum=$(sha256sum "$output"/"$filename" | cut -d' ' -f1)

            if [ "$checksum" = "$filechecksum" ]; then
                break
            fi

            echo "$filename failed, Redownloading"
            rm -f "$output"/"$filename"
        done

        rm -f "$output"/SHA256SUMS
    fi

    echo "Manjaro Done"
fi

# Ubuntu
if [[ $all -eq 1 || $ubuntu -eq 1 ]]; then
    echo "Downloading Ubuntu"
    release=$(curl -l --silent "$ubuntu_ftp"/ | sort -n -r | awk NR==1)

    if [ "$extension" == ".torrent" ]; then
        eval "$downloader" "$ubuntu_ftp"/"$release"/*.torrent -P "$output"
    else
        eval "$downloader" "$ubuntu_ftp"/"$release"/SHA256SUMS -P "$output"
        number=$(cat "$output"/SHA256SUMS | wc -l)

        for i in `seq 1 "$number"`; do
            while true; do
                filename=$(cut -d'*' -f2 "$output"/SHA256SUMS | awk NR=="$i")
                checksum=$(grep "$filename" "$output"/SHA256SUMS | cut -d' ' -f1)

                eval "$downloader" "$ubuntu_ftp"/"$release"/"$filename" -P "$output"
                filechecksum=$(sha256sum "$output"/"$filename" | cut -d' ' -f1)

                if [ "$checksum" = "$filechecksum" ]; then
                    break
                fi

                echo "$filename failed, Redownloading"
                rm -f "$output"/"$filename"
            done
        done

        rm -f "$output"/SHA256SUMS
    fi

    echo "Ubuntu Done"
fi

# Clean up index files that might of been downloaded
rm -f "$output"/index*